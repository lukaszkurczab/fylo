import React from 'react'
import illustrationIntro from "../images/illustration-intro.png"
import iconAccessAnywhere from "../images/icon-access-anywhere.svg"
import iconSecurity from "../images/icon-security.svg"
import iconCollaboration from "../images/icon-collaboration.svg"
import iconAnyFile from "../images/icon-any-file.svg"
import illustrationStayProductive from "../images/illustration-stay-productive.png"
import iconArrow from "../images/icon-arrow.svg"
import bgQuotes from "../images/bg-quotes.png"
import profile1 from "../images/profile-1.jpg"
import profile2 from "../images/profile-2.jpg"
import profile3 from "../images/profile-3.jpg"
import logo from"../images/logo.svg"
import iconLocation from"../images/icon-location.svg"
import iconPhone from"../images/icon-phone.svg"
import iconEmail from"../images/icon-email.svg"
import iconFb from "../images/icon-fb.png"
import iconTt from "../images/icon-tt.png"
import iconIg from "../images/icon-ig.png"

const Homepage = () => {
  return (
    <div className="wrapper">
      <img src="./images/bg-curvy-mobile.svg" alt="" className="bg__img"/>
  <header className="header">
    <img className="header__img" src="./images/logo.svg" alt=""/>
    <div className="header__nav">
      <p className="header__btn">Features</p>
      <p className="header__btn">Team</p>
      <p className="header__btn">Sign In</p>
    </div>
  </header>

  <main className="main">
    <img className="main__bigImg" src={illustrationIntro} alt=""/>
    <h1 className="main__bigTitle">All your files in one secure location, accessible anywhere.</h1>
    <p className="main__text">Fylo stores all your most important files in one secure location. Access them wherever you
      need, share and collaborate with friends family, and co-workers.</p>

    <button className="main__btn btn">Get Started</button>
    <div className="main__caps">
      <div className="main__cap">
        <img src={iconAccessAnywhere} alt="" className="main__img"/>
        <h2 className="main__title">Access your files, anywhere</h2>
        <p className="main__text">The ability to use a smartphone, tablet, or computer to access your account means your
          files follow you everywhere.</p>
      </div>

      <div className="main__cap">
        <img src={iconSecurity} alt="" className="main__img"/>
        <h2 className="main__title">Security you can trust</h2>
        <p className="main__text">2-factor authentication and user-controlled encryption are just a couple of the security
          features we allow to help secure your files.</p>
      </div>

      <div className="main__cap">
        <img src={iconCollaboration} alt="" className="main__img"/>
        <h2 className="main__title">Real-time collaboration</h2>
        <p className="main__text">Securely share files and folders with friends, family and colleagues for live
          collaboration.
          No email attachments required.</p>
      </div>

      <div className="main__cap">
        <img src={iconAnyFile} alt="" className="main__img"/>
        <h2 className="main__title">Store any type of file</h2>
        <p className="main__text">Whether you're sharing holidays photos or work documents, Fylo has you covered allowing
          for
          all
          file types to be securely stored and shared.</p>
      </div>
    </div>
    <div className="main__ideaBox">
      <img className="main__bigImg" src={illustrationStayProductive} alt=""/>
      <div className="main__ideaTextBox">
        <h3 className="main__idea">Stay productive, wherever you are</h3>

        <p className="main__ideaText">Never let location be an issue when accessing your files. Fylo has you covered for all
          of your
          file
          storage needs.</p>

        <p className="main__ideaText">Securely share files and folders with friends, family and colleagues for live
          collaboration. No email
          attachments required.</p>

        <p className="main__link">
          See how Fylo works <img className="main__linkImg" src={iconArrow} alt=""/></p>
      </div>
    </div>
    <div className="quotes">
      <div className="quotes__quote">
        <img className="quotes__img" src={bgQuotes} alt=""/>
        <p className="quotes__quoteText">
          Fylo has improved our team productivity by an order of
          magnitude. Since making the switch our team has become a well-oiled collaboration machine.
        </p>
        <div className="quotes__autor">
          <img src={profile1} alt="" className="quotes__autorImg"/>
          <div className="autor">
            <h4 className="quotes__autorName">Satish Patel</h4>
            <h5 className="quotes__autorPosition">Founder & CEO, Huddle</h5>
          </div>
        </div>
      </div>
      <div className="quotes__quote">
        <p className="quotes__quoteText">
          Fylo has improved our team productivity by an order of magnitude. Since making the switch our team has
          become a well-oiled collaboration machine.
        </p>
        <div className="quotes__autor">
          <img src={profile2} alt="" className="quotes__autorImg"/>
          <div className="autor">
            <h4 className="quotes__autorName">Bruce McKenzie</h4>
            <h5 className="quotes__autorPosition">Founder & CEO, Huddle</h5>
          </div>
        </div>
      </div>
      <div className="quotes__quote">
        <p className="quotes__quoteText">
          Fylo has improved our team
          productivity by an order of magnitude. Since making the switch our team has become a well-oiled
          collaboration
          machine.
        </p>
        <div className="quotes__autor">
          <img src={profile3} alt="" className="quotes__autorImg"/>
          <div className="autor">
            <h4 className="quotes__autorName">Iva Boyd</h4>
            <h5 className="quotes__autorPosition">Founder & CEO, Huddle</h5>
          </div>
        </div>
      </div>
    </div>
    <div className="earlyAccess">
      <h3 className="earlyAccess__title">Get early access today</h3>
      <p className="earlyAccess__text">It only takes a minute to sign up and our free starter tier is extremely generous.
        If
        you have any
        questions, our support team would be happy to help you.
      </p>
      <section className="earlyAccess__inputBox">
        <input type="text" className="earlyAccess__input" placeholder="email@example.com"/>
        <button className="earlyAccess__btn btn">Get Started For Free</button>
      </section>
    </div>
  </main>

  <footer className="footer">
    <img className="footer__img" src={logo} alt=""/>
    <div className="footer__contentBox">
      <div className="footer__contacts">
        <div className="footer__contact">
          <img className="footer__icon" src={iconLocation} alt=""/>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
          dolore magna aliqua
        </div>
        <div className="footer__contact">
          <img className="footer__icon" src={iconPhone} alt=""/>
          +1-543-123-4567
        </div>
        <div className="footer__contact">
          <img className="footer__icon" src={iconEmail} alt=""/>
          example@fylo.com
        </div>
      </div>
      <div className="links">
        <div className="footer__links">
          <h6 className="footer__link">About Us</h6>
          <h6 className="footer__link">Jobs</h6>
          <h6 className="footer__link">Press</h6>
          <h6 className="footer__link">Blog</h6>
        </div>
        <div className="footer__links">
          <h6 className="footer__link">Contact Us</h6>
          <h6 className="footer__link">Terms</h6>
          <h6 className="footer__link">Privacy</h6>
        </div>
      </div>

      <div className="socials">
        <img src={iconFb} alt="" className="socials__icon"/>
        <img src={iconTt} alt="" className="socials__icon"/>
        <img src={iconIg} alt="" className="socials__icon"/>
      </div>
    </div>
    <p className="attribution">
      Challenge by <a href="https://www.frontendmentor.io?ref=challenge">Frontend Mentor</a>.
      Coded by Łukasz Kurczab.
    </p>
  </footer>
    </div>
  )
}

export default Homepage